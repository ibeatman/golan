<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" ng-click="cancel()">&times;</span></button>
    <h4 class="modal-title">
        <span>עריכת פריט</span>
    </h4>
</div>

<div class="modal-body pic-edit">
    <form>
        <div class="form-group">
            <label>שם הפריט</label>
            <input type="text" class="form-control" ng-model="item.name">
        </div>
        <div class="form-group images">
            <label>תמונה</label>
            <div class="row">
                <div class="col-xs-6 col-md-3" ng-repeat="img in inatItem.observation_photos" ng-class="{active: ($index === selectedImg)}" ng-click="changeImg($index)">
                    <a href="#" class="thumbnail">
                        <img ng-src="{{img.photo.square_url}}">
                    </a>
                </div>
            </div>
        </div>
        <div class="tal">
            <button type="submit" class="btn btn-success" ng-click="save()">
                <span>שמירה</span>
                <i class="fa fa-spinner fa-spin" ng-show="loading"></i>
            </button>
        </div>
    </form>
</div>