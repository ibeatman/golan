<div class="panel" ng-class="{'panel-primary' : context==='edit', 'panel-success' : context ==='add' }">

    <div class="panel-heading">
        <span ng-show="context==='add'">הוספת מאמר</span>
        <span ng-show="context==='edit'">עריכה: {{item.subject}}</span>
    </div>

    <div class="panel-body">
        <form name="addArticle" novalidate>

            <div class="form-group" ng-class="{'has-error' : addArticle.subject.$invalid && addArticle.subject.$dirty}">
                <label class="control-label">נושא (כותרת)</label>
                <input type="text" name="subject" class="form-control" ng-model="item.subject" required>
                <div class="help-block" ng-show="addArticle.subject.$invalid && addArticle.subject.$dirty">חובה להזין כותרת למאמר</div>
            </div>

            <div class="form-group">
                <label class="control-label">תוכן</label>
                <textarea name="content" class="form-control" ng-model="item.content"></textarea>
            </div>

            <div class="form-group">
                <label class="control-label">מחבר</label>
                <input type="text" name="author" class="form-control" ng-model="item.author">
            </div>

            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label class="control-label">תאריך פרסום</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="published" uib-datepicker-popup="dd-MM-yyyy" ng-model="item.published" is-open="datePop.opened" show-weeks="false" datepicker-options="dateOptions" clear-text="נקה" current-text="היום" close-text="סגירה" />
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="datePop.opened = !datePop.opened"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">פורסם ב:</label>
                <input type="text" name="published_at" class="form-control" ng-model="item.published_at">
            </div>

            <div class="form-group">
                <label for="cats" class="control-label">קטגוריה</label>
                <select  class="form-control" ng-model="item.cat_id">
                    <option ng-repeat="cat in cats" value="{{cat.categories.id}}">{{cat.categories.name}}</option>
                </select>
            </div>

            <div class="form-group" ng-if="context==='add'">
                <label for="exampleInputFile" class="control-label">צירוף קובץ</label>
                <input type="file" id="exampleInputFile" name="file" file-model="item.file" accept=".pdf">
            </div>

            <button type="submit" class="btn btn-success" ng-click="save()">שמירה</button>
            <button type="submit" class="btn btn-default" ng-show="context==='edit'" ng-click="add()">ביטול</button>
        </form>
    </div>
</div>