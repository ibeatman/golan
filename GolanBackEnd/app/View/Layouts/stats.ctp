<!doctype html>
<html ng-app="golan">

<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>תצפיטבע</title>
	<base href="<?php echo $this->webroot; ?>">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">

	<link href='http://fonts.googleapis.com/earlyaccess/opensanshebrew.css' rel='stylesheet' type='text/css'>
	<?php echo $this->Html->css(array('/angular/styles/vendor','/angular/styles/app', 'stats')); ?>

</head>

<body>

<!--[if lt IE 10]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div ng-include="'app/components/header/header.html'"></div>
<div ui-view="main"></div>
<div ui-view="stats-view">
	<div class="main container-fluid">
		<?php echo $this->fetch('content'); ?>
	</div>
</div>
<?php

//$this->Js->set('golanBackEnd', $globalData);
echo $this->Js->writeBuffer(array('onDomReady' => false));


$debug_flag = 0;
$debug_flag = Configure::read('debug');
if( !$debug_flag) {
	// GA only if debug is off
	?>
	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-69970224-1', 'auto');

	</script>

	<?php
}   //  GA - debug only
echo $this->Html->script(array('/angular/scripts/vendor','/angular/scripts/app'));
echo $this->Html->script(array('highcharts','stats'));
?>

</body>
</html>