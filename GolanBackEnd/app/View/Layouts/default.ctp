<!doctype html>
<html ng-app="golan">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>תצפיטבע</title>
	<base href="<?php echo $this->webroot; ?>">

	<meta property="viewport" content="width=device-width">
	<meta name="keywords" content="קהילה טבע ניטור גולן מועצה אזורית תצפיטבע אפליקציה חיות בגולן" />
	<meta name="description" content="קהילה מנטרת טבע בגולן"/>
    <meta name="author" content="http://Carmel.Coop">
	<meta property="og:locale" content="he_IL" />
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="תצפיטבע" />
	<meta property="og:title" content="תצפיטבע">
	<meta property="og:description" content="קהילה מנטרת טבע בגולן">
	<meta property="og:image" content="<?php echo FULL_BASE_URL . '/angular/images/sideLogo.png'; ?>">


	<link href='http://fonts.googleapis.com/earlyaccess/opensanshebrew.css' rel='stylesheet' type='text/css'>
	<?php echo $this->Html->css(array('/angular/styles/vendor','/angular/styles/app')); ?>

</head>
<body>

<!--[if lt IE 10]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div ng-include="'app/components/header/header.html'"></div>
<div ui-view></div>
<?php
$this->Js->set('golanBackEnd', $globalData);
echo $this->Js->writeBuffer(array('onDomReady' => false));


$debug_flag = 0;
$debug_flag = Configure::read('debug');
if( !$debug_flag) {
// GA only if debug is off
	?>
	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
			a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-69970224-1', 'auto');

	</script>

	<?php
}   //  GA - debug only
echo $this->Html->script(array('/angular/scripts/vendor','/angular/scripts/app')); ?>
</body>
</html>
