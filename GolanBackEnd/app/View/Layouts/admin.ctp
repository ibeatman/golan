<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php


		echo $this->Html->meta('icon', 'favicon.ico', array('type' => 'icon'));
		echo $this->Html->css(array('../admin/dist/vendor', '../admin/dist/admin'));


		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

        if (!isset($bodyClass))
            $bodyClass = null;

	?>
</head>
<body role="document" class="<?php echo $bodyClass; ?>">
	<div ng-app="golanAdmin">
		<div class="fixed-notice"> <?php echo $this->Session->flash(); ?> </div>
		<!-- Fixed navbar -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <?php echo $this->Html->link("אתר תצפיטבע", "/", array('class' => "navbar-brand")); ?>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
					<?php
						// projects control
						echo "<li class=\"{$headerMenu['projects']}\">";
						echo $this->Html->link("פרוייקטים",
							"/control/projects/",
							array( 'class' => "dropdown-toggle", "data-vtoggle" => "dropdown", 'escape' => false)
							);
						echo "</li>";

						// news control
						echo "<li class=\"{$headerMenu['news']}\">";
						echo $this->Html->link("חדשות",
							"/control/news/",
							array( 'class' => "dropdown-toggle", "data-vtoggle" => "dropdown", 'escape' => false)
							);
						echo "</li>";

						// news control
						echo "<li class=\"{$headerMenu['links']}\">";
						echo $this->Html->link("קישורים",
							"/control/links/",
							array( 'class' => "dropdown-toggle", "data-vtoggle" => "dropdown", 'escape' => false)
							);
						echo "</li>";

						// about page gallery control
						echo "<li class=\"{$headerMenu['pics']}\">";
						echo $this->Html->link("גלריה",
							"/control/pics/",
							array( 'class' => "dropdown-toggle", "data-vtoggle" => "dropdown", 'escape' => false)
							);
						echo "</li>";

						// Curators control
						echo "<li class=\"{$headerMenu['curators']}\">";
						echo $this->Html->link("אוצרים",
							"/control/curators/"
							);
						echo "</li>";

						// Articles control
						echo "<li class=\"{$headerMenu['articles']}\">";
						echo $this->Html->link("מאמרים",
							"/control/articles/"
							);
						echo "</li>";

						// Texts control
						echo "<li class=\"{$headerMenu['texts']}\">";
						echo $this->Html->link("טקסטים",
							"/control/texts/"
							);
						echo "</li>";
					?>
					</ul>
					<ul class="nav navbar-nav pull-left">

					<?php

						// hello USER
						// echo "<li class=\"navbar-text\">שלום, {$currentUser['name']} </li>";
						// logout
						// echo "<li>" . $this->Html->link('יציאה',
						//     '/users/logout',
						//     array('class' => '', 'title' => 'יציאה מהמערכת') ). "</li>";
					?>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>

		<div class="container" ng-controller="appCtrl" role="main">

			<div id="content">
				<?php echo $this->fetch('content'); ?>
			</div>

		</div>

		<footer id="footer">
			<div class="container">
			<p class="copyrights">
				<span>
					<?php
					printf("גרסה: %s", Configure::read('coop_version'));
					?>

				</span>
				<span><?php echo $this->Html->link("Carmel.Coop",'http://carmel.coop/',array('target' => '_blank','style' => 'float:left;'));?></span>
			</p>
			<?php if (Configure::read('debug') > 0){ ?>
			<div class="panel panel-default" dir="ltr">
				<div class="panel-heading">Debug Panel</div>
				<?php echo $this->element('sql_dump'); ?>
			</div>
			<?php } ?>
			</div>
		</footer>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content"></div>
	  </div>
	</div>
	<?php echo $this->Html->script(array('../admin/dist/vendor','../admin/dist/admin.js')); 	?>
</body>
</html>
