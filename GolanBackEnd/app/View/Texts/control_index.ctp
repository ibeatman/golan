<?php

$intArr = array(
    'list'  => $texts,
    'urls'  => array(
        'list'  => $this->Html->url('/json/texts/index/true', true),
        'edit'   => $this->Html->url('edit', true)
    )
);

$this->Js->set('golanBackEnd', $intArr);
echo $this->Js->writeBuffer(array('onDomReady' => false));

?>
<div class="texts" ng-controller="TextsCtrl">
    <div class="page-header">
        <h3>טקסטים</h3>
    </div>


    <uib-tabset vertical="true" type="pills">
        <uib-tab ng-repeat="item in items" heading="{{translation[item.texts.slug] ? translation[item.texts.slug] : item.texts.slug }}">
            <h3>טקסט לעמוד {{translation[item.texts.slug] ? translation[item.texts.slug] : item.texts.slug }}</h3>
            <div class="content" ng-bind-html="trust(item.texts.content)" ng-show="!item.edit"></div>
            <div class="editing" ng-show="item.edit">
                <div text-angular ng-model="item.texts.content"></div>
            </div>
            <div class="btn-group margin-top-10" role="group">
                <button class="btn btn-default" ng-click="item.edit = !item.edit" ng-show="!item.edit">עריכה</button>
                <button class="btn btn-success" ng-click="save(item)" ng-show="item.edit">שמירה</button>
                <button class="btn btn-default" ng-click="item.edit = false" ng-show="item.edit">ביטול</button>
            </div>
        </uib-tab>
    </uib-tabset>
</div>