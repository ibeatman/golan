
<?php
App::uses('AppController', 'Controller');

class ArticlesController extends AppController {

	public function control_index()
	{
		$this->headerMenu['articles'] = 'active';
		$this->set('title_for_layout', 'מאמרים');

		$cats = $this->Article->query("SELECT * FROM categories");
		$this->set('cats',$cats);
	}

	// JSON API functions

	//  Add Article
	function control_add(){

		$this->layout = false;
		$data = $this->request->data;

		if ($this->request->is('post')) {
			$this->autoRender = false;

			//pr($data);
			$fileName = null;
			// process logo
			if( !empty($_FILES) )
			{
				// allowed mime / ext
				$allowed = array(
					'image/png' 		=> 'png',
					'image/gif' 		=> 'gif',
					'image/jpeg' 		=> 'jpg',
					'application/pdf'	=> 'pdf'
				);
				if( !empty($allowed[$_FILES['file']['type']]) )
				{
					// Good MIME
					$fileName = $name = $_FILES['file']['name'];
					move_uploaded_file($_FILES['file']['tmp_name'], WWW_ROOT . 'files'. DS .'articles' . DS . $name);

				}
				else
				{
					//	ToDo: Handle un allowed error
				}

				$data['Article']['file'] =  'files/articles/' . $fileName;
			}

			//  Updating?
			if(isset($this->request->data['id']))
				$this->Article->id = $this->request->data['id'];
			else //Creating
				$this->Article->create();


			if($this->Article->save($data)){
				return json_encode(array('success'=> true));
			}else {
				return json_encode(array('success'=> false));
			}
		}
	}

	//  Delete A Article
	function control_del($id = null) {
		$this->autoRender = false;

		if($id) {
			$this->Article->delete($id);
		}else{
			return 0;
		}

		return 1;
	}

	// Curator index
	//	$addCats = Attach categories to reponse ?
	function json_index($encode = true, $addCats = false)
	{


		$articles = $this->Article->find('all');
		// put them all in an anon array
		$ret = array();
		foreach( $articles as $A ) {
			$A['Article']['category'] = isset($A['Category']) ? $A['Category'] : null;
			$ret[] = $A['Article'];
		}

		//	Add Categories if needed
		if($addCats){

			$cats = $this->Article->query("SELECT * FROM categories");

			$ret = array(
				'articles'	=> $ret,
				'cats'		=> $cats
			);
		}

		// return json array of Curators
		return $encode ? json_encode($ret) : $ret;
	}
}