<?php
App::uses('AppController', 'Controller');

class PicsController extends AppController {

	// public $components = array('Session');

	// Admin Functions

	// Admin index
	public function control_index() 
	{
		$this->headerMenu['pics'] = 'active';
		$this->Pic->recursive = -1;
		$pics = $this->Pic->find('all');
		$this->set('pics', $pics);
		$this->set('title_for_layout', 'ניהול גלריית תמונות');
	}

	//  Add Pic (item)
	function control_add(){

		$this->layout = false;
		$data = $this->request->data;

		if(isset($data['id'])){
			$this->autoRender = false;

			//	Prepere date for saving
			$saveData = array(
				'id'	=> $data['id'],
				'url'	=> $data['ccImgUrl']
			);

			//	Get the fucker name
			$saveData['name'] = 'לא ידוע';

			if(isset($data['taxon']))
			{
				if(isset($data['taxon']['common_name']))
					$saveData['name'] = $data['taxon']['common_name']['name'];
				else
					$saveData['name'] = $data['taxon']['name'];
			}
			else if(isset($data['species_guess']))
				$saveData['name'] = $data['species_guess'];



			$this->Pic->create();
			if($this->Pic->save($saveData)){
				return json_encode(array('success'=> true));
			}else {
				return json_encode(array('success'=> false));
			}
		}
	}

	function control_del($id = null){
		$this->autoRender = false;

		if($id) {
			$this->Pic->delete($id);
		}

		$this->redirect('index');
	}

	function control_edit($id = null){

		$this->layout = false;
		$data = $this->request->data;

		if($id){
			if(isset($data['action']) && $data['action'] == 'save'){
				$this->autoRender = false;

				$this->Pic->id = $data['item']['id'];
				if($this->Pic->save($data['item']))
					return true;
				else
					return false;
			}
		}else {
			//  No Item to edit
			$this->redirect('index');
		}
	}

	function control_save_order(){
		$this->autoRender = false;
		$errors = array();
		$items = $this->request->data('items');

		if(isset($items[0]) && !empty($items[0])){
			foreach($items as $item){

				$this->Pic->id = $item['id'];
				if(!$this->Pic->save($item)){
					//  If Saving is fail, let me know
					$errors[] = $item['id'];
				};
				$this->Pic->clear();
			}
		}

		if(empty($errors))
			return true;
		else
			return json_encode(array(
				'error' => true,
				'ids'   => $errors
			));
	}

	/**
	 * JSON API functions	 *
	 */

	// Pics index
	function json_index($encode = true)
	{
		$this->Pic->recursive = -1;
		$proj = $this->Pic->find('all', array(
			'order' => array('Pic.order', 'Pic.name DESC'),
			)
		);
		// put them all in an anon array
		$ret = array();
		foreach( $proj as $P )
			$ret[] = $P['Pic'];
		// return json array of projects
		return $encode ? json_encode($ret) : $ret;
	}
}