<?php
App::uses('AppController', 'Controller');

class NewsController extends AppController {

	// public $components = array('Session');

	// Admin Functions

	// Admin index
	public function control_index()
	{
		$this->headerMenu['news'] = 'active';
		$this->News->recursive = -1;
		$news_all = $this->News->find('all');
		$this->set('news', $news_all);
		$this->set('title_for_layout', 'ניהול חדשות');
	}

	//  Add News
	function control_add(){

		$this->layout = false;
		$data = $this->request->data;

		if ($this->request->is('post')) {
			$this->autoRender = false;

			//  Updating?
			if(isset($this->request->data['id']))
				$this->News->id = $this->request->data['id'];
			else //Creating
				$this->News->create();


			if($this->News->save($data)){
				return json_encode(array('success'=> true));
			}else {
				return json_encode(array('success'=> false));
			}
		}
	}

	//  Delete A News
	function control_del($id = null) {
		$this->autoRender = false;

		if($id) {
			$this->News->delete($id);
		}else{
			return 0;
		}

		return 1;
	}

	// JSON API functions

	// news index
	function json_index($encode = true, $limit = -1, $page = 1, $year = null)
	{

		$this->News->recursive = -1;

		$conditions = array();
		if(!empty($year)){
			$conditions['YEAR(updated_at)'] = $year;
		}
		$news = $this->News->find('all', array(
			'limit' => $limit,
			'page'  => $page,
			'order' => array('News.updated_at DESC'),
			'conditions'    => $conditions
		));
		// put them all in an anon array
		$ret = array();
		foreach( $news as $N )
			$ret[] = $N['News'];

		if($encode){
			$newsCount =  $this->News->find('count');
			return json_encode(array(
				'items' => $ret,
				'total' => $newsCount
			));

		}
		// return json array of news
		return $ret;
	}

	function json_get($id = null){
		$news = $this->News->findById($id);

		return json_encode($news['News']);
	}

	function json_count(){
		$Q = "SELECT COUNT(*) as cnt, DATE_FORMAT(updated_at,'%Y') as year FROM news GROUP BY DATE_FORMAT(updated_at,'%Y') order by updated_at ASC;";
		$count = $this->News->query($Q);
		$output = array();
		foreach($count as $c){
			$output[] = $c[0];
		}

		return json_encode($output);
	}


	function og_view($id){

		$meta = array();

		$news = $this->News->findById($id);

		//	pr($news);

		//  Set Item Title
		$meta['og:title'] = "עדכון מתצפיטבע: ";
		if (isset($news['News']['headline']))
			$meta['og:title'] .= $news['News']['headline'];


		//  Description
		$meta['og:description'] = '';
		if (isset($news['News']['short']))
			$meta['og:description'] .= $news['News']['short'];

		//	Image - Use default logo
		$meta['og:image'] = FULL_BASE_URL . '/angular/images/sideLogo.png';

		$meta['og:description'] .= "\n";
		$meta['og:description'] .= 'תצפיטבע - קהילה מנטרת טבע בגולן.';

		$this->set('meta', $meta);
	}
}
