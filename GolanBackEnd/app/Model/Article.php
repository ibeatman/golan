<?php
App::uses('AppModel', 'Model');

class Article extends AppModel {

    public $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'cat_id'
        )
    );
}