(function() {
  
  'use strict';
  var partialsUrl = 'app/components/directives/partials/';
  angular.module('golan')

    //  Directive to get Item Grade
    .directive('itemGrade', function ($timeout, $compile) {

      var grades = [
        {
          title: 'מאושר',
          class: 'fa-check-circle',
          name: 'curator'
        },
        {
          title: 'זוהה ע"י קהילה',
          class: 'fa-comment',
          name: 'community'
        },
        {
          title: 'זוהה ע"י משתמש',
          class: 'fa-user',
          name: 'user'
        },
        {
          title: 'מחכה לזיהוי',
          class: 'fa-question-circle',
          name: 'needid'
        }
      ];

      function link($scope, element, $attr) {
        $scope.grades = grades;
        $scope.btnGroupType = 'btn-group-vertical'; // Vertical or Horizonal
        $scope.showPopover = false;


        if ($attr.horizonal) {
          $scope.btnGroupType = 'btn-group';
          $scope.showPopover = true;
        }
        else if (element.is('label')) {
          element.html('{{grades[activeGrade].title}}');
          $compile(element.contents())($scope);
        }

        $scope.$watch('data', function () {
          if ($scope.data.ccInfo) {
            $scope.activeGrade = $scope.data.ccInfo.grade;
          }
        });
      }

      return {
        scope: {data: '='},
        templateUrl: partialsUrl + 'item-grade.html',
        link: link
      };
    })

    //  Directive to get calculated item name
    .directive('itemsGallery', function (ccUtils) {

      return {
        scope: {
          items: '='
        },
        controller: function ($scope) {

          $scope.getImage = function (item) {
            return ccUtils.getItemImage(item, 'medium');
          };
        },
        link: function ($scope) {
          $scope.$watch('items', function () {
          });
        },
        templateUrl: partialsUrl + 'items-gallery.html'
      };
    })

    .directive('itemsList', function ($state) {

      return {
        scope: {
          items: '=',
          toggle: '&',
          activeItem: '='
        },
        controller: function ($scope) {
          $scope.clickLocation = function (item, index, active) {
            //  If Item is active on the list
            if(active){
              //  Navigate to obs view page
              $state.go('obs.single', {id: item.id});
            }else {
              //  Not, Make It active
              $scope.toggle({item: item, index: index});
            }

          };
        },
        link: function ($scope, element, attrs) {
        },
        templateUrl: partialsUrl + 'items-list.html'
      };
    })

    .directive('itemMedia', function (ccUtils, $compile) {

      return {
        restrict: 'C',
        scope: {item: '='},
        link: function ($scope, $element, $attr) {

          var size = $attr.size ? $attr.size : 'square',
              template;

          //  Get Image Url
          var imgUrl = ccUtils.getItemImage($scope.item, size);
          if (imgUrl) {
            template = "<img src='" + imgUrl + "' class='op-image' imageonload='removeOp'>";
          } else {
            //  No Image Found, return no-image div
            $element.addClass('no-image-parent');
            template = '<div class="no-image">תצפית<br>ללא<br>תמונה</div>';
          }


          var linkFn = $compile(template);
          var content = linkFn($scope);
          $element.append(content);
        }
      };

    })

    // Directive to run function when ENTER key (13) is press
    .directive('ccEnter', function () {
      return function (scope, element, attrs) {
        element.bind('keydown, keypress', function (event) {
          if (event.which === 13) {
            scope.$apply(function () {
              scope.$eval(attrs.ccEnter);
            });

            event.preventDefault();
          }
        });
      };
    })
    
    //  Directive For Fallback images
    .directive('actualSrc', function () {
      return {
        link: function postLink(scope, element, attrs) {
          attrs.$observe('actualSrc', function (newVal, oldVal) {
            if (newVal != undefined) {
              var img = new Image();
              img.src = attrs.actualSrc;
              angular.element(img).bind('load', function () {
                element.attr('src', attrs.actualSrc);
              });
            }
          });
        }
      };
    })

    //  Calc Item Wikipedia link
    .directive('ccWikiLink', function (ccUtils) {

      var title,
        wikiHeUrl = 'https://he.wikipedia.org/wiki/',
        buildUrl = function (title) {
          return wikiHeUrl + title.split(" ").join("_");
        };
      return {
        scope: {item: '=ccWikiLink'},
        link: function ($scope, $element) {

          $scope.$watch('item', function () {
            //  If item has taxon return its name (Latin)
            if ($scope.item.taxon) {
              $element.attr('href', buildUrl($scope.item.taxon.name));
              return;
            }

            //  Else Try to get item title
            if ($scope.item.ccInfo) {
              $element.attr('href', buildUrl($scope.item.ccInfo.name));
            }

          });
        }
      }
    })

    .directive('imageonload', function () {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {

          element.bind('load', function () {
            if (attrs.imageonload === 'removeOp') {
              element.removeClass('op-image');
            } else {
              scope.$apply(attrs.imageonload);
            }

          });
        }
      };
    })

    //  Password Confirm Compare Directive
    .directive("compareTo", function () {
      return {
        require: "ngModel",
        scope: {
          otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

          ngModel.$validators.compareTo = function (modelValue) {
            return modelValue == scope.otherModelValue;
          };

          scope.$watch("otherModelValue", function () {
            ngModel.$validate();
          });
        }
      };
    })

    //  Species Auto Compleate
    .directive('speciesCompleate', function (restApi) {

      var ctrl = function($scope){

        var vm = this;

        //  Prevent from no results function run
        var noResultsOnLoad = false;

        vm.text = '';
        vm.showClear = false;

        vm.clear = function(){
          vm.text = null;
          vm.taxa = null;
          vm.model = null;

          $scope.innerModel = null;
          $scope.noTaxasResults = false;
        };

        vm.setTaxaTypeHead = function (item) {
          $scope.innerModel = item.matched_term;
          vm.text = item.matched_term;
          vm.model = item.id;
          vm.taxa = item;
        };

        vm.getTaxa = function (val) {
          return restApi.taxa.autocomplete(val)
            .then(function (results) {
              return results.results;
            });
        };

        //  Do We have values setup on load?
        if(!$scope.model){
          $scope.model = '';
        }else {
          //  If so, Push them to our model
          noResultsOnLoad = true;
          vm.setTaxaTypeHead({
            matched_term: $scope.text ? $scope.text : $scope.model,
            id: $scope.model
          });

        }

        $scope.$watch('innerModel',function ( newValue ) {

          vm.text = newValue;

          if(newValue === null || !newValue || newValue.length === 0){
            vm.model = null;
            vm.taxa = null;
            vm.showClear = false;
          }else {
            vm.showClear = true;
          }
        });

        $scope.$watch('noTaxasResults', function (newValue) {

          if(noResultsOnLoad){
            noResultsOnLoad = false;
            return;
          }

          if(!newValue){
            vm.model = null;
            vm.taxa  = null;
          }
        })

      };

      return {
        scope         : {
          model : '=',
          text : '='
        },
        bindToController: {
          model       : '=?',  //  This Return a taxon_id
          taxa        : '=?',  //  This Return an Inat taxon object
          text        : '=?',  //  Hold the free text value
          onfocus     : '&',  //  On Foucus callback
          placeholder : '@'
        },
        controller    : ctrl,
        controllerAs  : 'auto',
        templateUrl   : partialsUrl + 'species-compleate.html'
      }
    })
      
    //  Users Auto Compleate
    .directive('usersCompleate', function (restApi) {

      var ctrl = function($scope){

        var vm = this;
 
        $scope.model = '';
        vm.text = '';
        vm.showClear = false;

        vm.clear = function(){
          vm.text = null;
          vm.taxa = null;
          vm.model = null;

          $scope.innerModel = null;
          $scope.noTaxasResults = false;
        };

        vm.set = function (item) {

          //  Test if user has name, if no, return to login
          var display = item.User.name ?  item.User.name : item.User.login;

          $scope.innerModel = display;
          vm.text = display;
          vm.model = item.User.id;
          vm.taxa = item;
        };

        vm.searchUsers = function (val) {

          return restApi.cake.members(val)
            .then(function (results) {
              return results;
            });
        };


        $scope.$watch("innerModel",function ( newValue ) {

          vm.text = newValue;

          if(newValue === null || !newValue || newValue.length === 0){
            vm.model = null;
            vm.taxa = null;
            vm.showClear = false;
          }else {
            vm.showClear = true;
          }
        });

        $scope.$watch('noTaxasResults', function (newValue) {
          if(!newValue){
            vm.model = null;
            vm.taxa  = null;
          }
        });


        //  User This model to connect with outside world
        $scope.internalControl = $scope.control || {};
        //  Clear function is use to let parent controller clear the model
        $scope.internalControl.clear = function() {
          $scope.innerModel = null;
        }
      };

      return {
        scope         : {
          control: '='
        },
        bindToController: {
          model       : '=?',  //  This Return a taxon_id
          taxa        : '=?',  //  This Return an Inat taxon object
          text        : '=?',  //  Hold the free text value
          onfocus     : '&',  //  On Focus callback
          placeholder : '@'

        },
        controller    : ctrl,
        controllerAs  : 'auto',
        templateUrl   : partialsUrl + 'users-compleate.html'
      }
    })
  ;

})();