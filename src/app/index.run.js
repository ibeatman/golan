(function() {
  'use strict';

  angular
    .module('golan')
    .run(runBlock);

  /** @ngInject */
  function runBlock($rootScope, $location, $window, $state, Notification) {

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState){

      //  If the requested View is from stats state (Controlled by cake)
      //  Reload the page to get new layout
      if(fromState.name === 'stats' || (toState.name === 'stats' && fromState.name.length)){
        $window.location.reload();

        return;
      }

      if (!$window.ga){
        return;
      }

      $window.ga('send', 'pageview', { page: $location.path() });

    });

    $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){

      if(angular.isDefined(error)){
        //  noAuth Error?
        if(error.type === 1) {
          $state.go('login', {
            redirect: error.redirect ? error.redirect : null,
            notify: error.notify ? error.notify : null
          });
        }else {
          if(error.notify){
            Notification.error(error.notify);
          }
          $state.go('home');

        }
      }else {
        $state.go('home');
      }
    });


    //  Add Browser Name and Version as body classes
    var get_browser = function() {

      var navigator = $window.navigator;

      var ua = navigator.userAgent,
          tem,
          M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

      if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name:'IE',version:(tem[1]||'')};
      }

      if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/);
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
      }

      M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];

      if ((tem=ua.match(/version\/(\d+)/i))!=null) {
        M.splice(1,1,tem[1]);

      }
      return {
        name: M[0],
        version: M[1]
      };
    },
        browser = get_browser();
    angular.element('body').addClass(browser.name + ' ' +  browser.name + '-' + browser.version);

  }

})();
