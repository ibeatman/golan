(function() {
  'use strict';

  angular.module('golan')
    .config(route)
    .controller('UsersController', ctrl)
    .controller('UserObsController', userObsCtrl)
  ;

  function route($stateProvider) {
      $stateProvider
        .state('user.obs', {
          url: '',
          controller: 'UserObsController',
          templateUrl: 'app/pages/users/partials/user.obs.html'
        })
        .state('user.projects', {
          url: '/projects',
          templateUrl: 'app/pages/projects/index.html',
          controller: 'ProjectsIndexController'
        })
        .state('user.password', {
          url: '/password',
          templateUrl: 'app/pages/users/partials/profile/user.password.html',
          controller: 'ProfileCtrl',
          resolve: {
            //  make Sure only Auth users access this state
            //  globalData is set here, to force Globals to load before checking log Status
            userInfo: function ($q, AuthService, globalData, $state) {

              AuthService.onLoadStatus().then(
                function (data) {
                  deferred.resolve(data);
                },
                function () {
                  deferred.reject({
                    type: 1,
                    redirect: function (info) {
                      $state.go('user.password', {slug: info.login})
                    },
                    notify: 'כדי להחליף סיסמא יש להתחבר קודם'
                  });
                }
              );

              var deferred = $q.defer();

              return deferred.promise;
            }
          }
        })
        .state('user.profile', {
          url: '/profile',
          templateUrl: 'app/pages/users/partials/profile/user.profile.html',
          controller: 'ProfileCtrl',
          resolve: {
            //  make Sure only Auth users access this state
            //  globalData is set here, to force Globals to load before checking log Status
            userInfo: function ($q, AuthService, globalData, $state) {

              AuthService.onLoadStatus().then(
                function (data) {
                  deferred.resolve(data);
                },
                function () {
                  deferred.reject({
                    type: 1,
                    redirect: function (info) {
                      $state.go('user.profile', {slug: info.login})
                    },
                    notify: 'על מנת לערוך את הפרופיל משתמש, יש להתחבר למערכת קודם'
                  });
                }
              );

              var deferred = $q.defer();

              return deferred.promise;
            }
          }
        })
    }

  function ctrl($scope, $stateParams, restApi, userInfo) {

      var userSlug = $stateParams.slug;

      $scope.user = null;
      $scope.showUserMenu = false;
      $scope.userProjects = null;

      var getUserProjects = function () {
        restApi.users.getUserProjects({userSlug: userSlug}).then(
          function (data) {
            $scope.userProjects = data;
          }
        )
      };

      var getUser = function () {
        //  Get
        restApi.users.getSingle({
            userSlug: userSlug
          })
          .then(
            function (data) {
              $scope.user = data;
              if (userInfo && userInfo.login === userSlug) {
                //console.log('Menu Should be sown');
                $scope.showUserMenu = true;
              }

              //  Get User Projects, Only if user is logged in
              if (userInfo) {
                getUserProjects();
              }
            },
            function () {
              $scope.showUserMenu = false;
            }
          );
      };

      getUser();

      $scope.$on('loginChanged', function (event, status, info) {
        $scope.userLoggedIn = status;
        if (status) {
          userInfo = info;
          getUser();
        } else {
          $scope.showUserMenu = false;
        }
      });

    }

  function userObsCtrl($scope, $stateParams, restApi) {

    var userSlug = $stateParams.slug,
      getItems = function () {
        restApi.obs.item.get({
          cache: false,
          id: userSlug,
          page: $scope.pagOptions.currentPage,
          perPage: $scope.pagOptions.perPage
        })
          .then(
            function (data) {
              $scope.items = data.items;
              $scope.pagOptions.totalItems = data.total;
            },
            function () {
            }
          );
      };

    $scope.pagOptions = {
      currentPage: 1,
      totalItems: 0,
      perPage: 15,
      pageChanged: function () {
        getItems();
      }
    };

    $scope.items = [];

    getItems();

  }

})();