(function() {
'use strict';

  angular.module('golan')
    .config(route)
    .controller('NewsController', news)
    .controller('NewsSingleController', single)
  ;

  
  function route($stateProvider) {

    $stateProvider
      .state('news.list', {
        url: '',
        templateUrl: 'app/pages/news/news.html',
        controller: 'NewsController'
      })

      .state('news.single', {
        url: '/view/:id',
        templateUrl: 'app/pages/news/single.html',
        controller: 'NewsSingleController'
      })
    ;
  }

  function news($scope, restApi) {

    $scope.items = null;
    $scope.currentPage = 1;
    $scope.perPage = 10;
    $scope.totalItems = 0;
    $scope.years = [];
    $scope.activeYear = null;
    $scope.pageChanged = function(){
      getItems();
    };

    $scope.filterYear = function(year){
      if(year === $scope.activeYear){
        $scope.activeYear = null;
      }else {
        $scope.activeYear = year;
      }

      getItems();
    };

    var getItems = function(){
      restApi.cake.news({
        limit: $scope.perPage,
        page: $scope.currentPage,
        year: $scope.activeYear
      })
        .then(
          function(data) {
            $scope.items = data.items;
            $scope.totalItems = data.total;
          },
          function(){}
        );
    };
    var getYears = function(){
      restApi.cake.newsCount().then(function(data){
        $scope.years = data;
      });
    };
    getItems();
    getYears();
  }

  function single($scope, restApi, $stateParams) {

    var newsId = $stateParams.id;
    var getNews = function(){
      restApi.cake.singleNews(newsId).then(function(data){
        $scope.item = data;
      });

    };

    $scope.item = {};
    getNews();
  }

})();