(function() {
  'use strict';

  angular.module('golan')
    .directive('registerForm', registerForm)
    .directive('backCheckEmpty', backCheckEmpty) 
    ;
    
  function registerForm(AuthService, Notification) {
      
      return {
        templateUrl: 'app/pages/register/register-form.html',
        controller: function ($scope, $state) {

          $scope.userLoggedIn = false;
          $scope.loading = false;
          $scope.user = {
            email: null,
            description: null,
            login: null,
            password: null,
            name : null
          };
          
          AuthService.getUserInfo().then(
            function () {
              $scope.userLoggedIn = true;
            }
          );
          
          
          $scope.save = function (form) {
            
            $scope.errors = null;
            //  Manualy make sure all fileds are checked
            angular.forEach(form.$error.required, function (field) {
              field.$setDirty();
            });
            
            if (form.$valid && !form.$untouched) {
              
              //console.log('All Valid', $scope.user);
              
              //  Do Saving
              $scope.loading = true;
              
              AuthService.registerUser($scope.user).then(
                function () {
                  $scope.loading = false;
                  $scope.success = true;
                  Notification.success('פרופיל נשמר בהצלחה');
                  $state.go('home');

                },
                function (data) {
                  $scope.loading = false;
                  $scope.errors = data.errors;
                  Notification.error('הרשמה נכשלה');
                }
              );
            }
            
            return;
            
          };
        }
      };
    }
    
  //  Check If user name exist on INAT
  function backCheckEmpty($q, restApi) {
      return {
        require: 'ngModel',
        scope: {},
        link: function (scope, element, attributes, ngModel) {
          
          var requestOptions = {
            loadingBar: false,
            userSlug: null
          };
          
          ngModel.$asyncValidators.backCheckEmpty = function (modelValue) {
            if (modelValue) {
              var deferred = $q.defer();
              requestOptions.userSlug = modelValue;
              restApi.users.getSingle(requestOptions).then(
                //  Found User, Reject
                function () {
                  deferred.reject();
                },
                //  User NOT found, Resolve
                function () {
                  deferred.resolve();
                }
              );
              
              
            }
            return deferred.promise;
          };
        }
      };
    }
  
})();