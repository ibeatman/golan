(function() {

  'use strict';

  angular.module('golan')
    .controller('loginController', loginController)
    ;

  function loginController($scope, AuthService, $stateParams, $state, userInfo, Notification) {
    $scope.params = $stateParams;

    //  If User is logged in, he should not be here!
    AuthService.getUserInfo().then(function () {
      $state.go('home');
      Notification.success('אתה מחובר למערכת');
    });

    $scope.userInfo = {
      user: null,
      password: null
    };

    $scope.authError = false;
    $scope.loading = false;

    $scope.login = function () {
      $scope.authError = false;
      $scope.loading = true;
      AuthService.login($scope.userInfo).then(
        function (info) {
          $scope.loading = false;
          if ($scope.params.redirect) {

            //  Redirect param can be function or a string
            if (angular.isFunction($scope.params.redirect)) {
              //  If function, tun it
              $scope.params.redirect(info);
            } else {
              //  If String navigate to it
              $state.go($scope.params.redirect);
            }
          } else {
            $state.go('home');
          }
        },
        function () {
          $scope.authError = true;
          $scope.loading = false;
        }
      );
    };
  }

})();