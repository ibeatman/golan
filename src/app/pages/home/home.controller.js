(function() {
  'use strict';
    
  angular
    .module('golan')
    .controller('HomeCtrl', homeCtrl)
  ;
  
  function homeCtrl($scope, $element, $window, restApi, $rootScope) {
      
    $rootScope.$broadcast('ccActiveState', 'home');

    //  Filters Vars - header Controller send those in event
    var taxonFilter = null,
        queryFilter = null,
        projectFilter = null,
        advSearch = null;

    //  Maps Options
    $scope.mapOptions = {
      center: {lat: 33.00603, lng: 35.48344}, // Golan Area
      zoom: 10,
      options: {
        scrollwheel: true,
        draggable: true,
        disableDefaultUI: false
      }
    };

    //  Global Scope Vars
    $scope.loadingItems = true;
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.perPage = 50;

    $scope.mapHeight = {height: $window.innerHeight - $element.offset().top};
    $scope.items = [];
    $scope.activeItem = null;


    var getItems = function () {

      //  Some Resetting
      $scope.items = [];
      $scope.loadingItems = true;

      var queryOb = {
        page: $scope.currentPage,
        perPage: $scope.perPage,
        taxonId: taxonFilter,
        project: angular.isObject(projectFilter) ? projectFilter.slug : null,
        q: queryFilter,
        advSearch: advSearch
      };

      //  Get
      restApi.obs.golanArea
        .get(queryOb)
        .then(
          function (data) {
            //console.log(data);

            //  Scroll To Top of Items list
            $('.scroller').animate({scrollTop: 0}, '80');

            $scope.activeItem = null;
            $scope.loadingItems = false;
            $scope.items = data.items;
            $scope.totalItems = data.total;

          }
        )
      ;
    };

    //  Show Item: set it to active, Sidebar & map
    $scope.showItem = function (index, listChange) {
      if ($scope.items[index].latitude && listChange) {
        $scope.mapOptions.center = {
          lat: $scope.items[index].latitude,
          lng: $scope.items[index].longitude
        };
      }
      $scope.activeItem = index;
    };

    //Pagination Logic
    $scope.pageChanged = function (page) {

      //console.log('Page Changed To: ', page);
      $scope.currentPage = page;
      getItems();

    };

    //  Header Filter Has Changed
    $scope.$on('headerFilter', function (event, filters) {
      
      taxonFilter   = filters.taxonId;
      queryFilter   = filters.query;
      projectFilter = filters.project;
      advSearch     = filters.advSearch;

      getItems();

      //  Scroll To top
      $('.scroller').animate({scrollTop: 0}, '1000');
    });

    //  Marker Has been Clicked on map, Highlight
    $scope.$on('activeChange', function (event, itemIndex) {
      $scope.showItem(itemIndex);
      //  Scroll To element
      $('.scroller').animate({scrollTop: itemIndex * 87}, '2000');
    });

    // Get Me some Items!
    getItems();

  }
  
})();