(function() {
'use strict';

  angular.module('golan')
    .directive('linksList', ctrl)
  ;
  
  function ctrl(restApi) {

    var link  = function($scope){

      $scope.items = null;

      var getItems = function(){
        restApi.cake.links()
          .then(
            function(data) {
              $scope.items = data;
            },
            function(){}
          );
      };

      //  Get intro Text
      $scope.text = '';
      restApi.cake.texts('links').then(function(text){
        $scope.text = text;
      });

      getItems();
    };

    return {
      scope: {},
      link: link,
      templateUrl: 'app/pages/info/links/links.html'
    };
  }
})();