(function() {
  'use strict';
  
  angular.module('golan')
    .controller('AboutCtrl', ctrl)
  ;
  
  function ctrl($scope, restApi, $sce, $timeout) {
        
        $scope.items = null;
        $scope.aboutBox = {
          headline: "אודות תצפיטבע",
          class: 'about',
          short: ''
        };
        $scope.newsCols = [[], [], [$scope.aboutBox]];
        
        //  Get into Text
        restApi.cake.texts('about').then(function (text) {
          $scope.aboutBox.short = $sce.trustAsHtml(text);
        });
        
        //  Get join Text
        restApi.cake.texts('about_bottom').then(function (text) {
          $scope.joinText = text;
        });
        
        var imgLoadedCounter = 0;
        
        var newsOrder = function (newsItems) {
          angular.forEach(newsItems, function (item, counter) {
            $scope.newsCols[counter % 2].push(item);
          });
        };
        
        var getHomeData = function () {
          restApi.cake.homeItems()
            .then(function (data) {
                $scope.items = data.pics;
                newsOrder(data.news);
              }
            );
        };
        
        getHomeData();

        //  Load Slick Slide Only when all images has been loaded
        $scope.imgLoad = function () {
          //  Count This Load event.
          imgLoadedCounter++;
          
          //  all Done?
          if (imgLoadedCounter === $scope.items.length) {

            $timeout(function(){

              $('.cc-slick').slick({
                slideToScroll: 1,
                slidesToShow: 3,
                arrows: true,
                variableWidth: true,
                rtl: false,
                infinite: true,
                centerMode: false,
                autoplay: true,
                autoplaySpeed: 2000
                //initialSlide: 3
              });
              $scope.slideReady = true;
            },1000);
          }
        };
      }
  
})();
