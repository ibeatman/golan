(function() {
  'use strict';
  
  
  angular.module('golan')
    .directive("obAddComment", obAddCommentDirective);
  
  function obAddCommentDirective(restApi, AuthService) {

    var controller = function ($scope) {

      var vm     = this;



      vm.cece = function(){
        console.log('ddd');

        console.log(vm.commentForm);
      };

      vm.show    = false;
      vm.loading = false;
      vm.comment = {
        type: 'comment',
        parent: vm.parent
      };

      var shouldShow = function () {

        //  Reset
        vm.show = false;

          //  Test
        AuthService.getUserInfo().then(function(){
          vm.show = true;
        });
      };

      shouldShow();

      //  Send Function
      vm.send = function (form) {

        vm.loading = true;
        
        //  Manualy make sure all fileds are checked
        angular.forEach(form.$error.required, function (field) {
          field.$setDirty();
        });
        
        //  All Ok, Send the fucker
        if (form.$valid) {


          if(vm.comment.specie === null){
            vm.comment.type = 'comment';
          }

          //  Send Data to restAPI, use dynamic function based on comment type
          restApi.inat[vm.comment.type](vm.comment).then(
            function () {
              
              vm.loading = false;

              //  Clear
              vm.comment.body   = null;
              vm.comment.specie = null;
              vm.activeTaxa     = null;

              vm.commentForm.$setPristine();

              //  Update Item
              $scope.$emit('updateItem');

            },
            function () {
              vm.loading = false;
              alert('שגיאה בשליחה הודעה');
            }
          );
          
        } else {
          vm.loading = false;
        }
      };
  
      $scope.$on('loginChanged', shouldShow);
    };
    
    return {
      restrict          : "E",
      scope             : true,
      bindToController  : { parent: '@' },
      controller        : controller,
      controllerAs      : 'addComment',
      templateUrl       : 'app/pages/obs/single/comments/addComment.html'
    };
        
  }
  
})();