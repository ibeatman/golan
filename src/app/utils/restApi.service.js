(function() {
  'use strict';
  
  angular
    .module('golan')
    .factory( 'restApi', restApi)
  ;

  function restApi($http, config, $q, $filter, Globals, ccUtils, $rootScope, ccSmartNotify) {

      var globals       = Globals.get(),
          url           = config.apiUrl,
          v1Api         = 'http://api.inaturalist.org/v1/',
          inatProxy     = 'inat/',
          golanChoords  = {
            swlat : 32.660819,
            swlng : 35.551178,
            nelat : 33.460714,
            nelng : 35.917847
          },
          utils         = {
            //  This Function is called before returing item to controllers, prepere data to views
            fixObs : function(items){

              var calcItemName  = function(item){
                  if(item.taxon){

                    //  Has Common Name?
                    if(item.taxon.common_name){
                      return item.taxon.common_name.name;
                    }else {
                      return item.taxon.name;
                    }

                  }
                  else {  //  No Taxon return segested name
                    if (item.species_guess){
                      return item.species_guess;
                    }else if(item.identifications && item.identifications.length) {
                      return item.identifications[0].taxon.name;
                    }else {
                      // No Name at all
                      return 'לא ידוע';
                    }
                  }
                },
                calcUserName  = function(item){
                  var user = {
                    id : item.user_id
                  };
                  if(angular.isObject(item.user)){
                    if(item.user.name) {
                      user.name = item.user.name;
                    }else {
                      user.name = item.user.login;
                    }
                  }else {
                    user.name = item.login;
                  }

                  return user;
                },
                calcGrade     = function(item){

                  //  Grades type are defained at itemGrade directive (direvtive.js)
                  //  ToDo: If curator Approved
                  //console.log(item);
                  var lastCuratorIdent = null;

                  if(item.identifications && item.identifications.length){
                    //  Get Last Curator Identification, Loop All identifications
                    angular.forEach(item.identifications, function(ident){

                      if(ccUtils.isUserCurator(ident.user.login)){
                        lastCuratorIdent = ident;
                      }
                    });

                    if(lastCuratorIdent){

                      // Check if our curator taxa is the same as inat taxa
                      if(lastCuratorIdent.taxon_id !== item.taxon_id){
                        //  Nope, need to set this curator identification as main taxa
                        //  Return Taxa object, parent function will fix this.
                        return lastCuratorIdent.taxon;
                      }
                      //  Cuarator taxa is the same, just return grade;
                      return 0;
                    }
                  }

                  if(!item.taxon_id || item.id_please){
                    return 3; //  Need Id
                  }else if(item.community_taxon_id){
                    return 1; //  Community
                  }
                  //  User
                  return 2;

                };

              var output = [];

              angular.forEach(items, function(item){

                item.ccInfo = {};

                item.ccInfo.grade = calcGrade(item);

                //  If returned grade is an object, need to set the object as the item taxon
                if(angular.isObject(item.ccInfo.grade)){
                  item.taxon = item.ccInfo.grade;
                  item.ccInfo.grade = 0;
                }

                item.ccInfo.name  = calcItemName(item);
                item.ccInfo.user  = calcUserName(item);

                this.push(item);

              }, output);

              return output;
            }
          },
          globalTexts   = [];


      $rootScope.$on('globals', function(){
        globals = Globals.get();
      });

      var items = {

        obs : {

          golanArea: {
            get: function(options){
              var deferred  = $q.defer(),
                params    = {
                  q: options.q || null,
                  projects    : options.project,
                  swlat       : 32.660819,
                  swlng       : 35.551178,
                  nelat       : 33.460714,
                  nelng       : 35.917847,
                  page        : options.page,
                  'per_page'  : options.perPage || 30,
                  extra       : 'fields,identifications,projects',
                  'taxon_id'  : options.taxonId,
                  locale      : 'iw'
                };

              //console.log(options);
              if(options.advSearch && options.advSearch.active){
                var advParam = {
                  d1    : $filter('date')(options.advSearch.startDate, 'yyyy-MM-dd'),
                  d2    : $filter('date')(options.advSearch.endDate, 'yyyy-MM-dd'),
                  user  : options.advSearch.user ? options.advSearch.user : null
                };

                angular.extend(params, advParam);
              }

              //  Test: Get Data from local
              if(config.test) {
                $http.get('../jsons/home.json').then(function(data){
                  deferred.resolve({items : utils.fixObs(data.data),total : 30});
                });
                return deferred.promise;
              }



              $http({
                method  : 'GET',
                url     : inatProxy + 'observations.json',
                cache   : true,
                params  : params
              })
                .success(function (data, status, headers) {
                  deferred.resolve({
                    items : utils.fixObs(data),
                    total : headers()['x-total-entries']
                  });
                })
                .error(function (data, status) {
                  deferred.reject('Error',data, status);
                });

              return deferred.promise;
            }
          },

          project: {
            get: function (options) {
              //console.log(options);
              var deferred = $q.defer();

              $http({
                method: 'GET',
                url: inatProxy + 'observations.json',
                params: {
                  q: options.q || '',
                  projects    : options.project,
                  page        : options.page,
                  'per_page'  : options.perPage || 30,
                  extra       : 'fields,identifications,projects',
                  locale      : 'iw'
                }
              })
                .success(function (data, status, headers) {

                  deferred.resolve({
                    items : utils.fixObs(data),
                    total : headers()['x-total-entries']
                  });
                })
                .error(function (data, status) {
                  deferred.reject('Error',data, status);
                });


              return deferred.promise;
            }
          },
  
          /**
           * 
           * @param options {id}
           * @returns {promise|r.promise|*|d.promise|a|i}
           */
          single : function(options){
            var deferred = $q.defer();

            if(options.id && options.id.length) {
              $http({
                method: 'GET',
                url: inatProxy + 'observations/'+ options.id +'.json',
                cache   : false,
                params: {
                  extra     : 'fields,identifications',
                  locale    : 'iw'
                }
              })
              .success(function (data, status, headers) { 
                deferred.resolve({
                  items : utils.fixObs([data])[0]
                });
              })
              .error(function (data, status) {
                deferred.reject('Error',data, status);
              });

            }else {
              deferred.reject('No Item Id');
            }
            return deferred.promise;
          },

          item : {
            get: function (options) {

              var deferred = $q.defer();

              if(options.id && options.id.length) {
                $http({
                  method  : 'GET',
                  url     : inatProxy + 'observations/'+ options.id +'.json',
                  cache   : angular.isDefined(options.cache) ? options.cache : true,
                  params: {
                    page      : options.page || 1,
                    'per_page': options.perPage || 30,
                    extra     : 'fields,identifications',
                    locale    : 'iw'
                  }
                })
                .success(function (data, status, headers) {

                  deferred.resolve({
                    items : utils.fixObs(data),
                    total : headers()['x-total-entries']
                  });
                })
                .error(function (data, status) {
                  deferred.reject('Error',data, status);
                });

              }else {
                deferred.reject('No Item Id');
              }
              return deferred.promise;
            }
          },

          similar : function(item){

            var deferred  = $q.defer(),
              results   = [],
              count     = 4,  //  How many items
              query = golanChoords;
            var thisResolved = function(){

              var log     = [],
                output  = [];

              //  Remove Current Ob, and Doops
              output = results.filter(function( obj ) {

                //  Check Current, Check Doops
                if(obj.id === item.id || log.indexOf(obj.id) > -1 || log.length >= count) {return false;}

                log.push(obj.id);

                return true;

              });

              //  Resolve
              deferred.resolve({items : output});
            };

            //  To Query, we need item, and item taxa id
            if(item.item && angular.isNumber(item.item.taxon_id)) {

              item = item.item;

              query.per_page = count + 1;
              query.taxon_id = item.taxon_id;
              var reqOptions = {
                method: 'GET',
                url: inatProxy + 'observations.json',
                params: query
              };

              $http(reqOptions)
                .success(function (data) {
                  results = data;

                  //  If Inat Found enthuh results, return
                  if(results.length > count+1){
                    thisResolved();
                  }else {

                    //  Item has parent taxon? Do Second Query
                    if(item.taxon_id && item.taxon.parent_id){

                      reqOptions.params.taxon_id = item.taxon.parent_id;
                      $http(reqOptions)
                        .success(function (data) {
                          results = results.concat(data);
                          thisResolved();
                        })
                        .error(function (data, status) {
                          deferred.reject('Error',data, status);
                        });
                    }
                    //  No Parent Taxon, return first query results
                    else{
                      thisResolved();
                    }
                  }
                })

                .error(function (data, status) {
                  deferred.reject('Error',data, status);
                });

            }else {
              deferred.reject('No Item set, Or no tax');
            }

            return deferred.promise;
          }

        },

        projects : {
          get: function (options) {
            //console.log(options);

            var deferred = $q.defer();

            $http({
              method: 'GET',
              url: url + 'projects/'+ options.project +'.json'
            })
              .success(function (data) {
                deferred.resolve(data);
              }).
            error(function (data, status) {
              deferred.reject('Error', data, status);
            });


            return deferred.promise;
          },

          //  Find Out Project slug by project id parameter
          getSlugById: function (id) {

            var deferred = $q.defer();

            $http({
              method: 'GET',
              url: v1Api + 'projects/'+ id
            })
              .success(function (data) {
                deferred.resolve(data.results[0].slug);
              }).
            error(function (data, status) {
              deferred.reject('Error', data, status);
            });


            return deferred.promise;
          },

          /**
           *
           * @param options {projectId, per_page, page, cache (bool)}
           * @returns {promise|*|r.promise|d.promise|a|i}
           */
          members: function(options) {

            var deferred = $q.defer();

            if(!options.projectId){
              deferred.reject('No Project Id');
              return deferred.promise;
            }

            var params = {
              per_page : options.per_page ? options.per_page : 4,
              page     : options.page ? options.page : 1
            };

            $http.get(v1Api + 'projects/'+ options.projectId +'/members', {
              params  : params,
              cache   : options.cache ? options.cache : false
            })
              .success(function (data) {
                deferred.resolve(data);
              })
              .error(function (data, status) {
              deferred.reject('Error', data, status);
            });


            return deferred.promise;
          }
        },

        users : {
          getSingle: function(options){

            var deferred = $q.defer();

            $http({
              method: 'GET',
              url: url + 'users/'+ options.userSlug +'.json',
              cache   : false,
              params: {},
              ignoreLoadingBar: options.loadingBar ? options.loadingBar : true
            })
              .success(function (data) {
                if(data.login){
                  data.ccInfo = { curator : ccUtils.isUserCurator(data.login) };
                }
                deferred.resolve(data);
              })
              .error(function (data, status) {
                deferred.reject('Error', data, status);
              });


            return deferred.promise;
          },

          getUserProjects: function(options){
            var deferred = $q.defer();
            $http({
              method: 'GET',
              url: url + 'projects/user/'+ options.userSlug +'.json',
              tokenRequired: true
            })
              .success(function(data){
                  deferred.resolve(data);
                }
              );

            return deferred.promise;
          }
        },

        stats: {
          mostViewed : function(query){
            var deferred = $q.defer(),
              defaultQuery = {
                projects : 'tatzpiteva',
                locale   : 'iw',
                per_page : 5
              },
              params = angular.extend(defaultQuery, query);

            $http({
              method: 'GET',
              //url: url + 'observations/taxon_stats.json',
              url: v1Api + 'observations/species_counts',
              params: params,
              cache: true
            })
              .success(function (data) {
                deferred.resolve(data);
              })
              .error(function (data, status) {
                deferred.reject('Error', data, status);
              });


            return deferred.promise;
          },

          topObservers : function(query){

            var deferred = $q.defer(),
                defaultQuery = {
                  project_id : 'tatzpiteva',
                  per_page    : 5
                },
                params = angular.extend(defaultQuery, query);

            $http({
              method: 'GET',
              cache: true,
              //url: url + 'observations/user_stats.json',
              url: v1Api + 'observations/observers',
              params: params
            })
              .success(function (data) {
                deferred.resolve(data);
              })
              .error(function (data, status) {
                deferred.reject('Error', data, status);
              });


            return deferred.promise;
          },

          topIdentifiers : function(query){

            var deferred = $q.defer(),
                defaultQuery = {
                  project_id : 'tatzpiteva',
                  per_page    : 5
                },
                params = angular.extend(defaultQuery, query);

            $http({
              method: 'GET',
              cache: true,
              url: v1Api + 'observations/identifiers',
              params: params
            })
              .success(function (data) {
                deferred.resolve(data);
              })
              .error(function (data, status) {
                deferred.reject('Error', data, status);
              });


            return deferred.promise;
          }
        },

        taxa : {

          autocomplete: function(query){

            var deferred = $q.defer();

            $http({
              method: 'GET',
              url: v1Api + 'taxa/autocomplete',
              params: {
                'per_page': 30,
                locale    : 'iw',
                q         : query
              },
              cache: true,
              ignoreLoadingBar: true
            })
              .success(function (data) {
                deferred.resolve(data);
              })
              .error(function (data, status) {
                deferred.reject('Error', data, status);
              });

            return deferred.promise;
          },

          get : function(id){
            $http.get(v1Api + 'taxa/' + id).then(
              function(data){},
              function(){}
            );
          }
        },

        //  Cakephp BackEnd
        cake: {
          //  About Page Data
          homeItems : function(){
            var deferred = $q.defer();
            $http
              .get(globals.backUrl + '/app/about' )
              .then(function(data) {
                deferred.resolve(data.data);
              });

            return deferred.promise;
          },

          links : function(){
            var deferred = $q.defer();
            $http
              .get(globals.backUrl + '/links' )
              .then(function(data) {
                deferred.resolve(data.data);
              });

            return deferred.promise;
          },

          sendContact: function(contact){

            var deferred = $q.defer();

            $http({
              method: 'POST',
              url: globals.backUrl + '/app/send_contact',
              data: $.param(contact),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              ignoreLoadingBar: true
            })
              .then(
                function(data) { deferred.resolve(data.data);},
                function(data) { deferred.reject(data.data);}
              );
            return deferred.promise;
          },

          news: function(options){
            var deferred = $q.defer(),
              page     = options.page ? options.page : 1,
              limit    = options.limit ? options.limit : 10,
              year     = options.year ? options.year : 0;

            $http({
              method: 'GET',
              url: globals.backUrl + '/news/index/true/' + limit + '/' + page + '/' + year,
              cache: true
            })
              .success(function(data) {
                deferred.resolve({
                  items: data.items,
                  total: data.total
                });
              });

            return deferred.promise;
          },

          newsCount: function(){
            var deferred = $q.defer();
            $http.get(globals.backUrl + '/news/count').then(
              function(data){
                deferred.resolve(data.data);
              },
              function(data){
                deferred.reject(data);
              }
            );
            return deferred.promise;
          },

          singleNews: function(newsId){
            var deferred = $q.defer();
            $http.get(globals.backUrl + '/news/get/' + newsId).then(
              function(data){
                deferred.resolve(data.data);
              },
              function(data){
                deferred.reject(data);
              }
            );
            return deferred.promise;
          },

          articles: function(options){
            var deferred = $q.defer(),
              getCats  = true;

            $http({
              method: 'GET',
              url: globals.backUrl + '/articles/index/true/' + getCats,
              cache: true
            })
              .success(function(data) {
                deferred.resolve({
                  items: data.articles,
                  cats: data.cats
                });
              });

            return deferred.promise;
          },

          //  Get Text For app
          texts: function(textSlug) {

            var deferred = $q.defer();

            //  If We didnt get the texts before, Get Them from cake
            //  Else return text slug from cache
            if(!globalTexts.length){
              $http({
                method: 'GET',
                url: globals.backUrl + '/texts',
                cache: true
              })
                .success(function(data) {
                  globalTexts = data;
                  deferred.resolve(globalTexts[textSlug]);
                });
            }else {
              deferred.resolve(globalTexts[textSlug]);
            }

            return deferred.promise;
          },

          //  User Deletes his own identification
          deleteIdentification : function (id) {

            var deferred = $q.defer();

            $http.post( globals.backUrl + '/obs/del_identification/' + id)
              .success(function(data){ 
                deferred.resolve(data);
              })
              .error(function(){
                deferred.reject(data);
              });

            return deferred.promise;
          },

          deleteOb : function(id){

            var deferred = $q.defer();

            $http.post( globals.backUrl + '/obs/delete/' + id)
              .success(function(data){
                deferred.resolve(data);
              })
              .error(function(){
                deferred.reject();
              });

            return deferred.promise;
          },
          
          members : function (name) {
  
            var deferred = $q.defer();
  
            $http.get( globals.backUrl + '/users/search/' + name)
              .success(function(data){
                deferred.resolve(data);
              })
              .error(function(){
                deferred.reject();
              });

            return deferred.promise;
          }
        },

        //  Functions that runs directly Against Inat using proxy
        inat: {
          addOb : function(obData, obImages){

            //console.log('Inat Add Ob', obData);

            var deferred = $q.defer(),
                self = this;

            $http({
              method : 'POST',
              tokenRequired: true,
              data : {
                observation : obData
              },
              url : 'inat/observations.json'
            })
              .then(function(data) {

                var ob = data.data[0];

                //  We Have An Ob, Add projects
                self.obsToProject(ob.id, obData.projects).then(function () {
                  deferred.resolve(ob);
                });

                //  Upload Files
                if(obImages.length){
                  self.obImages(ob.id, obImages);
                }
              },function(data) {
                deferred.reject(data);
              });

            return deferred.promise;
          },

          editOb : function(obData, obImages){

            //console.log('Inat Add Ob', obData);

            var deferred = $q.defer(),
                self = this;

            $http({
              method : 'POST',
              tokenRequired: true,
              data : {
                _method: "put",
                ignore_photos: 1,
                observation : obData
              },
              url : 'inat/observations/'+ obData.id +'.json'
            })
              .then(function(data) {

                var ob = data.data[0];
                deferred.resolve(ob);

                //  We Have An Ob, Add projects
                if(obData.projectsToAdd.length){
                  self.obsToProject(ob.id, obData.projectsToAdd).then(function () {  });
                }
                //  Need To Remove projects?
                if(obData.projectsToRemove.length){
                  self.obsRemoveProject(ob.id, obData.projectsToRemove).then(function () {  });
                }

                deferred.resolve(ob);
                //  Upload Files
                if(obImages.length){self.obImages(ob.id, obImages);}
                
              },function(data) {
                deferred.reject(data);
              });

            return deferred.promise;
          },

          /**
           *
           * @param projects - Array
           * @param obId - int
           * @returns {Promise}
           */
          obsToProject: function (obId, projects) {
            var deferred = $q.defer();

            var promises = [];

            angular.forEach(projects, function(projectId){
              var def = $q.defer();

              $http({
                method: "POST",
                url : 'inat/project_observations.json',
                tokenRequired: true,
                data : {
                  project_observation : {
                    observation_id  : obId,
                    project_id      : projectId
                  }
                },
                ignoreLoadingBar: true

              }).then(function(data){
                def.resolve(data);
              },function(e){
                console.log(e);
                def.reject(e);
              });

              this.push(def.promise);
            }, promises);

            $q.all(promises).then(function(data){
              deferred.resolve();
            });

            return deferred.promise;
          },

          /**
           *
           * @param projects - Array
           * @param obId - int
           * @returns {Promise}
           */

          // ==== NOT WORKING =====
          obsRemoveProject: function (obId, projects) {
            var deferred = $q.defer();

            var promises = [];

            angular.forEach(projects, function(projectId){
              var def = $q.defer();

              console.log('Removing: ', projectId);
              $http({
                method: "DELETE",
                url : 'inat/projects/'+ projectId +'/remove?observation_id='+obId,
                tokenRequired: true,
                ignoreLoadingBar: true

              }).then(function(data){
                def.resolve(data);
              },function(e){
                console.log(e);
                def.reject(e);
              });

              this.push(def.promise);
            }, promises);

            $q.all(promises).then(function(data){
              deferred.resolve();
            });

            return deferred.promise;
          },

          /**
           *
           * @param images - Array
           * @param obId - int
           * @returns {Promise}
           */
          obImages: function (obId, images) {
            var deferred = $q.defer();

            var promises = [];

            //  Prevent User from leaving page while uploading is in progress
            ccUtils.preventWindowLeave(true, 'תמונות שקשורות לתצפיות לא סיימו לעלות לאתר. נא להשאיר את העמוד פתוח עד סיום ההעלאה');

            angular.forEach(images, function(image){
              //console.log("Loading File" ,image);
              var def = $q.defer();

              var fd = new FormData();
              fd.append('file',image.file);
              fd.append('observation_photo[observation_id]', obId);

              $http({
                method: "POST",
                url : 'inat/observation_photos.json',
                tokenRequired: true,
                ignoreLoadingBar: true,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                data :  fd
              })
                .then(function(data){
                  //console.log("File Loaded",data);
                  def.resolve();
                }, function (e) {
                  console.log(e);
                  def.reject();
                });

              //  Push Defered to promisess array
              this.push(def.promise);

            }, promises);


            ccSmartNotify.notify(promises);

            $q.all(promises).then(function(){

              //  Remove window loading preventing
              ccUtils.preventWindowLeave(false);

              //  Notify App that were finished (ob controller will use this to reload ob images)
              $rootScope.$broadcast('CCImagesLoaded');

              deferred.resolve();
            });

            return deferred.promise;
          },

          /**
           *
           * @param identification {parent, specie, body}
           * @returns {r.promise|promise|*|a|s}
           */
          identification: function (identification) {
  
            var deferred = $q.defer(),
                fixData = {
                  observation_id : identification.parent,
                  taxon_id : identification.specie,
                  body: identification.body ? identification.body : null
                }
              ;

  
            $http({
              method : 'POST',
              tokenRequired: true,
              data : {
                identification : fixData
              },
              ignoreLoadingBar: true,
              url : 'inat/identifications.json'
            })
              .then(function(data) {
                deferred.resolve(data)
              },function(data) {
                deferred.reject(data);
              });
            
            return deferred.promise;
          },
  
  
          /**
           * User make a Comment On ob
           * @param data : {parent, body}
           * @returns {Promise}
           */
          comment: function(comment){
  
            var deferred = $q.defer();
  
            var fixData = {
              'comment[parent_type]'  : 'Observation',
              'comment[parent_id]'    : comment.parent,
              'comment[body]'         : comment.body
            };


            $http({
              method : 'POST',
              tokenRequired: true,
              data : fixData,
              ignoreLoadingBar: true,
              url : 'inat/comments.json'
            })
              .success(function(data){
                if(data.errors){
                  deferred.reject(data.errors);
                }else {
                  deferred.resolve(data);
                }
              })
              .error(function(){
                deferred.reject(data);
              });
  
            return deferred.promise;
          }
        }
      };

      return items;

    }
})();